export enum MoveType {
  up,
  down,
  top,
  bottom,
}

export enum ZoomMode {
  auto,
  width,
  height,
  full,
  disabled,
}
